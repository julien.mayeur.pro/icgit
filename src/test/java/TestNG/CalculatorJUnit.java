package TestNG;

import static org.junit.Assert.*;

import org.junit.Test;

import com.calculator.Calculator;

public class CalculatorJUnit {

	@Test
	public void test1() {
		assertEquals("error in add()", -5, Calculator.add(5,0));
	}

	@Test
	public void test2() {
		assertNotEquals("error in add()", 0, Calculator.add(1,2));
	}

	@Test
	public void test3() {
		assertEquals(0, Calculator.divInt(5,0));
	}

	@Test
	public void test4() {
		assertEquals(3, Calculator.divInt(10,5));
	}

	@Test
	public void test5() {
		assertEquals(2, Calculator.divInt(10,5));
	}

	@Test
	public void test6() {
		assertEquals(2.5, Calculator.divReal(5,2),0);
	}

}
